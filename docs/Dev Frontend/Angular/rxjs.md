# Opérateurs RxJS
[![BLOG](https://img.shields.io/badge/BLOG-orange?style=flat-square)](#)

Les opérateurs Rxjs sont des fonctions qui permettent de manipuler les valeurs émises par un observable. Ils sont utilisés pour effectuer des opérations sur les valeurs émises par un observable et retourner un nouvel observable. Il existe de nombreux opérateurs dans RxJS, mais nous allons voir les plus couramment utilisés.

Ils s'utilisent avec la méthode `pipe` de l'observable.

```typescript linenums="1" title="component.ts"
this.service.getDateJour()
    .pipe(
        map(date => date.toUpperCase())
    )
    .subscribe(date => {console.log(date);});

expected output: FRI AUG 25 2023
```

Pipe est une méthode qui permet de combiner plusieurs opérateurs opérateurs pour effectuer des opérations sur les valeurs émises par un observable.

## map

L'opérateur `map` permet de transformer les valeurs émises par un observable. Il prend une fonction de transformation en argument et retourne un nouvel observable qui émet les valeurs transformées.

```typescript linenums="1" title="service.ts"
import { of, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

getDateJour(): Observable<Date> {
    
    return of(new Date()).pipe(
        map(date => date.toDateString())
    ); 

}
```

permet de passer de la date du format "2023-08-25T08:00:00.000Z" à "Fri Aug 25 2023" grâce au `map`

```typescript linenums="1" title="component.ts"
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

this.service.getDateJour().pipe(
    map(date => date.toUpperCase())
).subscribe(date => {
    console.log(date);
});

expected output: FRI AUG 25 2023

```

permets de transformer la date en majuscule grâce au `map`

## filter

L'opérateur `filter` permet de filtrer les valeurs émises par un observable. Il prend une fonction de prédicat en argument et retourne un nouvel observable qui émet les valeurs qui satisfont le prédicat.

```typescript linenums="1" title="service.ts"
import { of, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

getNombresPairs(): Observable<number> {
    
    return of(1, 2, 3, 4, 5, 6).pipe(
        filter(nombre => nombre % 2 === 0)
    ); 
}

```

```typescript linenums="1" title="component.ts"
import { of } from 'rxjs';
import { filter } from 'rxjs/operators';

this.service.getNombresPairs().subscribe(nombre => {
    console.log(nombre);
});

expected output: 2, 4, 6

```

Dans l'exemple ci-dessus, nous avons utilisé l'opérateur `filter` pour filtrer les nombres pairs émis par l'observable.

## tap

L'opérateur `tap` permet d'effectuer des actions sur les valeurs émises par un observable sans les transformer. Il prend une fonction de rappel en argument et retourne un nouvel observable qui émet les mêmes valeurs que l'observable d'origine.

```typescript linenums="1" title="service.ts"
import { of, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

getNombresPairs(): Observable<number> {
    
    return of(1, 2, 3, 4, 5, 6).pipe(
        tap(nombre => console.log(`nombre émis: ${nombre}`)),
        filter(nombre => nombre % 2 === 0)
    ); 
}

```

```typescript linenums="1" title="component.ts"
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';

this.service.getNombresPairs().subscribe(nombre => {
    console.log(nombre);
});

expected output:
nombre émis: 1
nombre émis: 2
nombre émis: 3
nombre émis: 4
nombre émis: 5
nombre émis: 6
2, 4, 6

```

Dans l'exemple ci-dessus, nous avons utilisé l'opérateur `tap` pour afficher les nombres émis par l'observable sans les transformer.

## take

L'opérateur `take` permet de limiter le nombre de valeurs émises par un observable. Il prend un nombre entier en argument et retourne un nouvel observable qui émet les `n` premières valeurs de l'observable d'origine.

```typescript linenums="1" title="service.ts"
import { of, Observable } from 'rxjs';
import { take } from 'rxjs/operators';

getNombresPairs(): Observable<number> {
    
    return of(1, 2, 3, 4, 5, 6).pipe(
        take(3)
    ); 
}
```

```typescript linenums="1" title="component.ts"
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

this.service.getNombresPairs().subscribe(nombre => {
    console.log(nombre);
});

expected output: 1, 2, 3

```

Dans l'exemple ci-dessus, nous avons utilisé l'opérateur `take` pour limiter le nombre de valeurs émises par l'observable à 3.

## takeUntil

Avec `takeUntil`, l’observable principal arrête d’émettre lorsque l’observable secondaire commence. Ça peut être un timer, un Subject déclenché manuellement ou une autre source d’événements.



```typescript linenums="1" title="component.ts"
import { of, interval, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const source = interval(1000);
const timer$ = timer(5000);

const result = source.pipe(
  takeUntil(timer$)
);

const subscribe = result.subscribe(val => console.log(val));

expected output: 0, 1, 2, 3

```

source émet une valeur chaque secondes. timer$ émet une valeur après 5 secondes.
Dans l'exemple ci-dessus, nous avons utilisé l'opérateur `takeUntil` pour arrêter l'émission des valeurs de l'observable `source` après 5 secondes.


## distinctUntilChanged

L'opérateur `distinctUntilChanged` permet de filtrer les valeurs émises par un observable pour qu'il n'émette que les valeurs qui sont différentes de la valeur précédente. Il retourne un nouvel observable qui émet les valeurs qui sont différentes de la valeur précédente.

```typescript linenums="1" title="service.ts"
import { of, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

getNombresPairs(): Observable<number> {
    
    return of(1, 1, 1, 2, 3, 3, 4, 4, 4, 5, 4).pipe(
        distinctUntilChanged()
    ); 
}
```

```typescript linenums="1" title="component.ts"
import { of } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

this.service.getNombresPairs().subscribe(nombre => {
    console.log(nombre);
});

expected output: 1, 2, 3, 4, 5, 4

```

Dans l'exemple ci-dessus, nous avons utilisé l'opérateur `distinctUntilChanged` pour filtrer les valeurs émises par l'observable pour qu'il n'émette que les valeurs qui sont différentes de la valeur précédente.

## switchMap

L'opérateur `switchMap` permet de transformer les valeurs émises par un observable en un autre observable. Il prend une fonction de transformation en argument et retourne un nouvel observable qui émet les valeurs transformées.

```typescript linenums="1" title="service.ts"
import { of, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

const getCurrentCity = () => {
    /* Produce one value from the array every second. */
    return zip(
        from(['Strasbourg', 'Paris', 'Lyon']),
        interval(1000)
    )
        .pipe(map(([city]) => city));
};

const getTemperature = city => {
    /* Produce the same temperature per city every 500ms. */
    return interval(500)
        .pipe(map(() => 100 / city.length));
};
```

```typescript linenums="1" title="component.ts"
getCurrrentCity().subscribe(city => console.log(city));

# at 1s
Strasbourg 
# at 2s
Paris
# at 3s
Lyon

getTemperature('Lyon').subscribe(temperature => console.log(temperature));

25 # at  400ms
25 # at  800ms
25 # at 1200ms 
# ... same value every 400ms forever.

```

```typescript linenums="1" title="component.ts"
import {switchMap} from 'rxjs/operators';

const currentCityTemperature$ = getCurrentCity()
    .pipe(switchMap(city => getTemperature(city)));
​
currentCityTemperature$
    .subscribe(temperature => console.log(temperature));

# at 1000ms, we're in Strasbourg.
10 # at 1400ms, Strasbourg's temperature.
10 # at 1800ms, Strasbourg's temperature.
# at 2000ms, we're in Paris.
20 # at 2400ms, Paris' temperature.
20 # at 2800ms, Paris' temperature.
# at 3000ms, we're in Lyon.
25 # at 3400ms, Lyon's temperature.
25 # at 3800ms, Lyon's temperature.
25 # at 4200ms, Lyon's temperature.
...

```





