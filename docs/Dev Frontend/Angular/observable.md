# Observables
[![BLOG](https://img.shields.io/badge/BLOG-orange?style=flat-square)](#)

## Observables

L’Observable est le type d’objet principal de RxJS. Nous pouvons l’« écouter », ou l’« observer », pour réagir à ses émissions. Pour écouter ces émissions, il faut appeler la fonction subscribe() de l’Observable à l’endroit où vous pouvez accéder à la valeur émise. Il est impossible d’invoquer l’événement ou le changement de valeur à l’aide d’un simple Observable. Il faut le comprendre comme un objet en « lecture seule ». Pour cette raison, il est généralement conseillé d’exposer les Observables lorsque vous ne souhaitez pas que d’autres composants de l’application invoquent des événements, c’est-à-dire, quand vous voulez qu’ils écoutent seulement.

```typescript linenums="1" title="service.ts"
import { of, Observable } from 'rxjs';

getDateJour(): Observable<Date> {
    
    return of(new Date()); 
}

```

`of` de la librairie rxjs permet de créer un observable à partir d'une valeur

```typescript linenums="1" title="component.ts"
import { of } from 'rxjs';

this.service.getDateJour().subscribe(date => {
    console.log(date);
});

expected output: 2023-08-25T00:00:00.000Z

```

Dans l'exemple ci-dessus, nous avons un service qui retourne un observable de type Date. Dans le composant, nous avons appelé la méthode `getDateJour` du service et nous avons souscrit à l'observable retourné. Lorsque la date est émise, nous la recevons dans la fonction de rappel de la méthode `subscribe` et nous l'affichons dans la console.

!!! info "A savoir"
    Dans cet exemple, si on ne s'abonne pas à l'observable, la date ne sera pas émise. C'est le principe de la programmation réactive. Les observables ne sont pas exécutés tant qu'ils ne sont pas écoutés. Il faut aussi penser à se désabonner de l'observable pour éviter les fuites de mémoire. Pour cela, on peut utiliser la méthode `unsubscribe` de l'observable.

Désabonnement de l'observable grâce au hook `ngOnDestroy` du composant.

```typescript linenums="1" title="component.ts"

private subscription: Subscription;

ngOnInit() {
    this.subscription = this.service.getDateJour().subscribe(date => {
        console.log(date);
    });
}

ngOnDestroy() {
    this.subscription.unsubscribe();
}

```

## Operators

Les opérateurs sont des fonctions pures qui permettent de manipuler les valeurs émises par un observable. Ils sont utilisés pour effectuer des opérations sur les valeurs émises par un observable et retourner un nouvel observable. Il existe de nombreux opérateurs dans RxJS, mais nous allons voir les plus couramment utilisés.






## Subject

Un Subject est un type d'Observable qui permet de publier des valeurs à plusieurs observateurs. Il est possible de publier des valeurs à partir de l'extérieur de l'observable en appelant la méthode `next` de l'observable. Cela permet de transformer un Observable en un objet en lecture/écriture.

*Un Subject est un observable :* on peut s'abonner à un Subject comme à n'importe quel autre observable.
En interne, s'abonner à un Subject n'invoque pas une nouvelle exécution qui délivre des valeurs, Il enregistre simplement l'observateur dans une liste d'observateurs (Qui seront tous prévenu quand une nouvelle donnée se transmise).

*Un Subject est un observateur :* on peut transmettre une nouvelle valeur avec sa méthode next. Faites simplement next(value) et elle sera transmise à tous les observateurs enregistrés.

```typescript linenums="1" title="component.ts"
import { Subject } from 'rxjs';

subject = new Subject<string>();

subject.next('event 0');
  
subject.subscribe(event => console.log(event));  

subject.next('event 1');  

subject.next('event 2');  

subject.next('event 3');  

expected output: event 1, event 2, event 3

```

!!! info "A savoir"
    Un Subject n'a pas de valeur initiale. Il ne publie que les valeurs qui lui sont envoyées par la méthode `next`.


## BehaviorSubject

Un BehaviorSubject est un type de Subject qui a une valeur initiale. Il est similaire à un Subject, mais il a une propriété supplémentaire qui est la valeur actuelle. Il est nécessaire de lui fournir une valeur initiale lors de sa création.

```typescript linenums="1" title="service.ts"
import { BehaviorSubject } from 'rxjs';

behaviorSubject = new BehaviorSubject<string>('event 0');

behaviorSubject.subscribe(event => console.log(event));

behaviorSubject.next('event 1');

behaviorSubject.next('event 2');

behaviorSubject.next('event 3');

expected output: event 0, event 1, event 2, event 3

```



