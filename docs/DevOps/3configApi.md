# Configuration de l'API
[![TECHNIQUE](https://img.shields.io/badge/TECHNIQUE-red?style=flat-square)](#)

## Configuration de base sans docker

Nous allons prendre l'exemple d'une api NestJS

### Copie du projet sur le serveur

```bash
git clone lienDuProjet
```

### Installation des dépendances

```bash
cd nomDuProjet
npm install -g @nestjs/cli
npm install
```

### Build de l'application et lancement

```bash
nest build
cd dist
node main.js
```

!!! success "Votre API est en ligne !"
    Et voilà, votre API est en ligne ! Rien de plus simple, finalement c'est comme quannd vous êtes en local sur votre machine.

## Configuration avec docker

Nous allons créer deux conteneurs Docker, un pour l'API et un pour la base de données.

!!! tip
    Il faut savoir que deux conteneur docker qui n'ont pas la même image ne peuvent pas communiquer entre eux, pour cela nous allons utiliser un réseau docker.

### Création d'un réseau docker

```bash
docker network create mynetwork
```

Cela va créer un réseau docker nommé `mynetwork`, vous pouvez le nommer comme vous le souhaitez.

### Création du Dockerfile

A la racine de votre projet, créez un fichier `Dockerfile` (vous pouvez le mettre sur git)

```
FROM node:20

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node", "dist/main.js"]
```

Ce fichier permet de créer une image Docker pour votre application.

!!! info
    Pour rappel, une image Docker est un environnement de travail complet, qui contient tout ce dont votre application a besoin pour fonctionner. Elle permet de déployer et d'exécuter votre application dans n'importe quel environnement garantissant que votre application fonctionnera de la même manière partout.

### Création du docker-compose.yml

Créez un fichier `docker-compose.yml` à la racine de votre projet.

Il permet de lancer votre application en utilisant l'image Docker que vous avez créée, il ira la prendre à la racine directement.

```yaml
version: "3"
services:
  app:
    build: . # le point indique que le Dockerfile est à la racine
    ports:
      - "3000:3000"
    environment: # variables d'envrionnement à disposition dans le conteneur
      - POSTGRES_HOST=postgres
      - POSTGRES_PORT=5432
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=mysecretpassword
      - POSTGRES_DB=postgres
    networks:
      - mynetwork:

networks:
  - mynetwork:
    external: true # permet de dire que le réseau est déjà créé (sinon il le crée)
```

Ici, nous avons créé un service nommé `app` qui utilise le Dockerfile pour créer l'image Docker, il expose le port 3000 et utilise le réseau docker `mynetwork`.

### Lancement du conteneur

```bash
docker compose up
```

!!! success "Félicitations !"
    Et voilà, votre API est en ligne !

??? Example "Voici comment le faire sans `docker-compose.yml`, uniquement avec `docker run` et `docker build`"
    Il est tout à fait possible de lancer le conteneur directement avec une commande `docker run`,

    !!! warning
        Attention cette comande ne pourra pas build l'image directement, il faudra le faire manuellement avec `docker build`. A l'inverse, le fichier `docker-compose.yml` créé l'image automatiquement.

    ```bash
    docker build -t nomDeVotreImage .
    ```

    Ici vous choisissez le nom de votre image et elle sera contruite à partir du Dockerfile. Il faut bien penser à mettre le point à la fin de la commande et exécuter cette commande à la racine du projet pour utiliser le bon Dockerfile.

    l'équivalent en ligne de commande pour créer le conteneur serait:

    ```bash
    docker run --name app --network mynetwork -d -p 3000:3000 -e POSTGRES_HOST=postgres -e POSTGRES_PORT=5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_DB=postgres nomDeVotreImage
    ```

!!! warning
    Le nom d'HOST de la base de données doit être le même que celui que vous avez donné à votre conteneur de base de données. On voit ça juste après.

Cependant une api ne fonctionne pas sans base de données, voici comment la configurer.

### Base de données PostgreSQL

Ici aussi vous avez le choix, soit vous installez PostgreSQL directement sur votre serveur, soit vous utilisez un conteneur Docker.

!!! info
    Il faut savoir qu'en production, il n'est pas conseillé d'utiliser un conteneur Docker pour la base de données (point de vue performance), mais pour des raisons de simplicité, nous allons le faire ici.

Création d'un conteneur Docker pour la base de données PostgreSQL (Si vous utilisez une autre base de données, adaptez les commandes)

Ici nous allons générer un conteneur pour la base de données avec un fichier `docker-compose.yml`.

```yaml
services:
  postgres:
    image: "postgres:latest"
    ports:
      - 5432:5432
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: mysecretpassword
      POSTGRES_DB: postgres

    volumes:
      - ./postgresql:/var/lib/postgresql
      - ./postgresql_data:/var/lib/postgresql/data

    networks:
      - mynetwork:

networks:
  mynetwork:
    external: true
```

??? Example "Voici l'équivalent avec la commande `docker run`:"

    !!! tip
        Ici pas besoin de créer un fichier `Dockerfile`, l'image est déjà disponible sur le docker hub. postgres:latest est l'image publique de la dernière version de PostgreSQL.

    ```bash
    docker run --name postgres --network mynetwork -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_DB=postgres -v "$(pwd)/postgresql:/var/lib/postgresql" -v "$(pwd)/postgresql_data:/var/lib/postgresql/data" postgres:latest
    ```

Je ne vous explique pas comment configurer la base de données étant donné qu'il y a plusieurs types de base de données et que cela dépend de votre projet. (Pour mongo pas besoin de créer de base de données, elle se crée automatiquement, créer le docker suffit)

Pour cet exemple, lorsque votre api se connectera à vôtre base de données, elle ne pourra pas créer de bdd, vous devrez le faire manuellement (en vous connectant via un client comme pgAdmin par exemple, ou bien directement via le terminal).

Il est tout à fait possible de créer les deux conteneurs dans le même fichier `docker-compose.yml`, voici comment :

```yaml
services:
  app:
    build: .
    ports:
      - "3000:3000"
    environment:
      - POSTGRES_HOST=postgres
      - POSTGRES_PORT=5432
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=mysecretpassword
      - POSTGRES_DB=postgres
    networks:
      - mynetwork

  postgres:
    image: "postgres:latest"
    ports:
      - 5432:5432
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: mysecretpassword
      POSTGRES_DB: postgres
    volumes:
      - ./postgresql:/var/lib/postgresql
      - ./postgresql_data:/var/lib/postgresql/data
    networks:
      - mynetwork

networks:
  - mynetwork:
    external: true
```

Vous pouvez maintenant lancer les deux conteneurs avec la commande `docker compose up` à la racine de votre projet, où se trouvent les fichiers `docker-compose.yml` et `Dockerfile`.

!!! success "Félicitations !"
    Votre API et votre environnement de base de données sont en ligne et actifs !
