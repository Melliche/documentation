# Configuration SonarQube CI/CD
[![TECHNIQUE](https://img.shields.io/badge/TECHNIQUE-red?style=flat-square)](#)

Dans ce guide nous allons voir comment mettre en place SonarQube pour un projet gitlab.

Pour commencez soyez sûr de bien avoir docker d'installé. Sinon se référer à la documentation Configuration VPS.

!!! warning
    Pour pouvoir faire tourner sonarQube sur votre vs il est recommandé d'avoir au moins **4 gigas** de rams.

Ensuite vous pouvez directement créer un container en ligne de commande pointant sur l'image publique de sonar.

```bash
docker run -d --name sonar -p 9000:9000 sonarqube:lts-community
```

Ensuite vous pouvez vous rendre sur l'interface web de sonar à l'adresse suivante `http://IP_SERVEUR:9000`
Ca devrait donner ça :

![sonar](../images/sonarLogin.png)

Les identifiants par défaut sont `admin` et `admin`.

Vous devrez créer un nouveau mot de passe.

Ensuite vous devrez créé un nouveau projet mannuellement.

![sonar](../images/createManually.png)

Ensuite vous générez le nom du projet sonar

![sonar](../images/setupSonarName.png)

Vous selectionnez avec Gitlab CI

Vous devrez ensuite sélectionner le bon environnement de votre projet.

![sonar](../images/sonarProjectEnvironment.png)

Suivez toutes les instructions de la page

??? example "Voici un exemple de fichier `gitlab-ci.yml` pour un projet Angular"
    
    ```yaml
    stages:
        - build
        - test
        - deploy

    build:
        stage: build
        image: node:latest
        before_script: - npm install -g @nestjs/cli
        script: 
            - echo "BUILD FRONT" 
            - npm install -g @angular/cli 
            - npm ci 
            - npm install 
            - ng build --configuration production
        artifacts:
        paths: - dist/

    sonarqube-check:
        stage: test
        image:
        name: sonarsource/sonar-scanner-cli:latest
        entrypoint: [""]
        variables:
        SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
            GIT_DEPTH: "0"
        cache:
            key: "${CI_JOB_NAME}"
        paths: - .sonar/cache
        script: - sonar-scanner
        allow_failure: true

    deploy:
        stage: deploy
        ...
    ```

Une fois que c'est fait, vous pouvez push directement avec le fichier gitlab CI a jour et le fichier sonar-project.properties.



!!! info
    Vous devriez voir tous vos projets sur la page d'acccueil de sonarQube sur l'url `http://IP_SERVEUR:9000`

!!! success "Bravo"
    Vous avez réussi à mettre en place SonarQube pour votre projet gitlab.
