# Configuration du Serveur VPS
[![TECHNIQUE](https://img.shields.io/badge/TECHNIQUE-red?style=flat-square)](#)
## Introduction

Ce document a pour but de détailler la configuration d'un serveur VPS pour un projet de développement.

!!! note "Pour cet exemple"
    Nous resterons dans le cas d'un vps à l'usage de mise en production d'applications webs ainsi que d'apis et de base de données.

## Prérequis

- Un serveur VPS avec une distribution Linux (Ubuntu 22.04 LTS par exemple)

## Chose à savoir

Pour vous connecter à un vps vous avez besoin de son IP et du nom d'utilisateur
(Vous pouvez aussi utiliser des outils comme Putty, plus visuel et qui permet de sauvegarder des connections)

Dans n'importe quel terminal

```bash
ssh 'nomUtilisateur'@'IP'
exemple: ssh root@1.1.1.1
```

Il vous sera demandé le mot de passe de l'utilisateur, puis vous serez connecté au serveur.

## Configuration de base

### Mise à jour du système

Pour commencer, il est important de mettre à jour le système.

```bash
sudo apt update # mise à jour de la liste des paquets
sudo apt upgrade # mise à jour des paquets
```

### Création d'un utilisateur

Il est recommandé de ne pas utiliser le compte root pour les tâches quotidiennes. Il est préférable de créer un utilisateur dédié. (Pareil pour la mise en production par une CI/CD par exemple)

```bash
adduser 'nomUtilisateur'
```

Ajout des droits sudo à l'utilisateur

```bash
usermod -aG sudo 'nomUtilisateur'
```

### Installation de Node.js

Installer Node.js 20, nécessaire pour faire tourner les dernières versions de frameworks js.

```bash
curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -
sudo apt install -y nodejs
```

### Installation de Docker

Taper ces commandes une par une

```bash
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Si vous avez besoin de git, vous pouvez aussi l'installer.

```bash
sudo apt install git
```
