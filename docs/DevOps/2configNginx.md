# Configuration Nginx / Frontend
[![TECHNIQUE](https://img.shields.io/badge/TECHNIQUE-red?style=flat-square)](#)

Nginx est un serveur web, cela vous permettra de mettre en ligne votre application Frontend.

## Installation de Nginx

```bash
sudo apt install -y nginx
```

## Configuration de base

### Création répertoire pour l'application front

Créer un répertoire pour le build de votre application Angular, React, Vue, etc.
Vous pouvez aussi y mettre un simple fichier html

```bash
sudo mkdir -p /var/www/html/front
```

Imaginons que vous ayiez une application front angular à disposition sur git, vous pouvez la cloner à la racine de votre serveur.

```bash
git clone urlGit
```

Ensuite, vous pouvez build votre application et copier le contenu dans le répertoire que vous avez créé.

```bash
ng build
sudo cp -r dist/* /var/www/html/front
```

Ceci est un exemple pour une application angular, les commandes peuvent varier pour une application React ou Vue...

### Création d'un fichier de configuration

Une fois nginx installé, la bonne habitude est de créer un fichier de configuration à cet emplacement `/etc/nginx/conf.d/`.

Il servira à gérer le pointage de l'url de votre nom de domaine vers votre application frontEnd.

```bash
sudo nano /etc/nginx/conf.d/proxy.conf
```

### Configuration de Nginx http

Editer le fichier de configuration de Nginx "proxy.conf", le nom importe peu...

```nginx
server {
    listen 80; # port 80 pour le HTTP
    listen [::]:80;
    root /var/www/html/front/browser; # chemin vers le dossier de l'application Angular

    index index.html index.htm; # index.html est le fichier d'entrée de l'application Angular

    server_name nomDeDomaine; # nom de domaine de votre application
    location / {
        try_files $uri $uri/ =404;
    }
}
```

Dans l'exemple si dessus, browser est un sous-dossier du build de l'application Angular dans lequel se trouve le fichier index.html.

Ensuite il faut redémarrer Nginx pour que les modifications soient prises en compte.

Vérifier la syntaxe de la configuration Nginx

```bash
sudo nginx -t
```

Redémarrer Nginx

```bash
sudo systemctl restart nginx
```

!!! success "Succès"
    La mise en ligne du site sur le nom de domaine en http doit être effective **_instantanément_**.

!!! warning "Attention"
    Votre nom de domaine doit être configuré pour pointer vers l'adresse IP de votre serveur

### Configuration de Certbot

Certbot est un outil qui permet de générer des certificats SSL gratuit pour votre site web.

```bash
sudo apt install -y certbot python3-certbot-nginx
```

Une fois certbot installé, vous pouvez générer un certificat pour votre nom de domaine.

```bash
certbot --nginx -d nomDeDomain --redirect
```

Cela va vous demander de renseigner une adresse mail pour la récupération des certificats et d'accepter les conditions d'utilisation.

!!! info
    Une fois cela fait, tous les certificats seront générés et placé dans le dossier `/etc/letsencrypt/live/nomDeDomaine/`.

### Configuration de Nginx https

Editer le fichier de configuration de Nginx "proxy.conf" créé précédemment.

```nginx
server {

    root /var/www/html/angular/browser; # chemin vers le dossier de l'application Angular

    index index.html index.htm; # index.html est le fichier d'entrée de l'application Angular

    server_name degree-dealers.com; # nom de domaine

    location / { # configuration de la redirection
        try_files $uri $uri/ /index.html; # redirection vers index.html
    }

    # configuration du certificat SSL | port 443 pour le HTTPS
    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/degree-dealers.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/degree-dealers.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
    if ($host = degree-dealers.com) { # redirection vers le nom de domaine
        return 301 https://$host$request_uri; # redirection vers le HTTPS
    }

    listen 80;
    listen [::]:80;
    server_name degree-dealers.com www.degree-dealers.com;
    return 404;
}
```

Ensuite il faut penser à redémarrer Nginx pour que les modifications soient prises en compte.

Vérifier la syntaxe de la configuration Nginx

```bash
sudo nginx -t
```

Redémarrer Nginx

```bash
sudo systemctl restart nginx
```

!!! success "Succès"
    La mise en ligne du site sur le nom de domaine en **https** doit être effective **_instantanément_**.
