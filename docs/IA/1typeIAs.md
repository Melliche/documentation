# Types d'Intelligence Artificielle
[![BLOG](https://img.shields.io/badge/BLOG-orange?style=flat-square)](#)

Cette section présente les différents types d'intelligence artificielle (IA) existants, classifiés selon leurs capacités, leurs fonctionnalités et leurs applications spécifiques.

---

## 📚 **Classification des IA**
L'IA peut être classée de plusieurs façons :

1. **Selon leurs capacités** (IA faible, générale et super-intelligence).
2. **Selon leurs fonctionnalités** (réactive, mémoire limitée, théorie de l'esprit, consciente).
3. **Selon leurs applications** (générative, symbolique, décisionnelle, analytique, etc.).

---

## 🔎 **1. Classification par capacités / Niveau**

**Une grande majorité des spécialistes de l’IA s’accorde à dire qu’il existe 3 niveaux d’intelligence artificielle :**

**<u>ANI</u>** (Artificial Narrow Intelligence) <br>
**<u>AGI</u>** (Artificial General Intelligence) <br>
**<u>ASI</u>** (Artificial Super Intelligence)

### **1.1 Intelligence Artificielle Faible (ANI - Artificial Narrow Intelligence)**
L'IA faible se concentre sur des tâches spécifiques. Elle n'a pas conscience d'elle-même ni de capacités généralisées.

  - **Exemples** :
    - Assistants virtuels (Siri, Alexa)
    - Recommandations Netflix
    - Reconnaissance faciale

### **1.2 Intelligence Artificielle Générale (AGI - Artificial General Intelligence)**
Une IA capable de comprendre, d'apprendre et d'exécuter n'importe quelle tâche intellectuelle qu'un humain peut accomplir.

- **Exemple théorique** : IA avec des capacités cognitives équivalentes à un humain.
- **État actuel** : En développement, aucune AGI n'existe encore.

!!! info "A savoir"
    On pourrait même dire que le LLM GPT-4 est une Super ANI, sans pour autant être au niveau d’une Intelligence Générale Artificielle.

### **1.3 Super-Intelligence Artificielle (ASI - Artificial Super Intelligence)**
Une IA surpassant les capacités humaines dans tous les domaines, y compris la créativité, la résolution de problèmes et les interactions émotionnelles.

- **Statut** : Hypothétique et sujet de débats philosophiques et éthiques.
- **Risques** : Une ASI non contrôlée pourrait représenter une menace existentielle pour l'humanité.

---

## 🔧 **2. Classification par fonctionnalités**

### **2.1 IA Réactive**
L'IA réactive ne réagit qu'à des situations présentes. Elle ne conserve pas de données sur le passé pour influencer ses décisions.

- **Exemples** : Deep Blue, le programme d'échecs d'IBM.

### **2.2 IA à Mémoire Limitée**
Elle peut utiliser des données historiques pour améliorer ses résultats, mais elle ne construit pas de mémoire permanente.

- **Exemples** : Voitures autonomes.

### **2.3 Théorie de l'Esprit**
Un concept d’IA avancée qui serait capable de comprendre et de répondre aux émotions humaines.

- **Statut** : Concept futuriste, en cours de recherche.

### **2.4 IA Consciente**
Une IA qui possède une conscience de soi, capable de comprendre son propre état et celui des autres.

- **Statut** : Purement théorique.

---

## 🛠 **3. Classification par applications**

### **3.1 IA Générative**
L'IA générative peut créer du contenu original en fonction des données qu'elle a été formée à utiliser.

  - **Exemples** :
    - ChatGPT (texte)
    - DALL-E, MidJourney (images)
    - Synthèse musicale avec IA (Jukebox d'OpenAI)

### **3.2 IA Symbolique / Procédurale**
L'IA symbolique ou procédurale génère dynamiquement du contenu basé sur des règles algorithmiques ou probabilistes.

  - **Exemples** :
    - Génération de mondes ouverts dans les jeux vidéo (Minecraft, No Man's Sky).
    - Création automatique de niveaux.

### **3.3 IA Décisionnelle**
Elle analyse des scénarios complexes et propose des solutions optimales en fonction des données.

  - **Exemples** :
    - Systèmes d'aide à la décision dans la finance.
    - Diagnostic médical.

### **3.4 IA Analytique**
Conçue pour extraire des informations précises de larges ensembles de données.

  - **Exemples** :
    - Analyse de sentiment (avis clients).
    - Analyse prédictive (modélisation économique).

---

## 🚀 **Résumé des Types d'IA**

| **Classification**        | **Types d'IA**                                  | **Exemples**                                              |
|---------------------------|------------------------------------------------|---------------------------------------------------------|
| **Par capacités**         | - **ANI** (Intelligence Artificielle Étroitement Spécialisée) <br> - **AGI** (Intelligence Artificielle Générale) <br> - **ASI** (Super Intelligence Artificielle) | - **ANI** : Siri, ChatGPT (GPT-4), AlphaGo <br> - **AGI** : Concept futuriste (aucun exemple réel) <br> - **ASI** : Théorique (aucune existence actuelle) |
| **Par fonctionnalités**   | - Réactive <br> - Mémoire Limitée <br> - Théorie de l'Esprit <br> - Consciente | - Réactive : Deep Blue <br> - Mémoire Limitée : Voitures autonomes <br> - Théorie de l'Esprit : En recherche <br> - Consciente : Hypothétique |
| **Par applications**      | - Générative <br> - Symbolique/Procédurale (ou Basée sur des règles) <br> - Décisionnelle <br> - Analytique | - Générative : ChatGPT, DALL·E, GPT-4 <br> - Symbolique : Systèmes experts (comme en médecine) <br> - Décisionnelle : IA en finance, optimisation de flux <br> - Analytique : Outils de Business Intelligence |

---

