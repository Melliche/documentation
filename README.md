
*Pour consulter la documentation en ligne, suivez ce lien : [Documentation MkDocs](https://documentation-melliche-8d6acb2cae4cfacc081670cebfb649245c14808c.gitlab.io/)*


### Documentation MkDocs

Toutes les informations présentes dans ce dépôt proviennent de mes expériences personnelles, de documentations techniques ou de sources trouvées sur Internet.

## Engagements
- Chaque partie technique est **testée et validée** avant d'être publiée.
- Les documentations et blogs sont basés sur des **sources internet** et des **connaissances personnelles**.

## Identification des pages
En haut de chaque page, un indicateur en **rouge** précise si la page est :
- **Blog** : Contient des réflexions ou des contenus informatifs.
- **Technique** : Contient des exemples techniques testées et validées.

## Contributions
N'hésitez pas à proposer des **pull requests (PR)** pour :
- Corriger des erreurs.
- Ajouter des informations pertinentes.

## Transparence sur l'IA
Les textes n'ont **pas été rédigés par une IA**, sauf pour la **mise en forme** et la **clarification stylistique**.

### En local
Pour lancer la documentation en local, utilisez la commande suivante :
```bash
mkdocs serve
```

